<?php
/**
 * Function Custom meta box for Premium
 * 
 * @package Album and Image Gallery Plus Lightbox
 * @since 1.4.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="pro-notice"><?php echo sprintf( __( 'Upgrade to <a href="%s" target="_blank">Premium Version</a> to unlock more features.', 'album-and-image-gallery-plus-lightbox'), AIGPL_PLUGIN_LINK); ?></div>
<table class="form-table aigpl-metabox-table">
	<tbody>

		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Layouts', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('7 ( Album Grid, Album Slider, Gallery Grid, Gallery Slider, Album with Reload Gallery, Gallery Masonry, Album Masonry ).', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>
		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Designs', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('15+. In lite version only one design.', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>
		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('WP Templating Features ', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('You can modify plugin html/designs in your current theme.', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>
		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Shortcode Generator ', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Play with all shortcode parameters with preview panel. No documentation required.' , 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>

		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Album Image with Popup', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Display Album image with popup on click.', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>

		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Album and Gallery Masonry Style', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Display Masonry style for Album and Gallery.', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>

		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Gallery Image Link', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Display custom link to gallery image.', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>

		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Drag & Drop Slide Order Change', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Arrange your desired slides with your desired order and display.' , 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>
		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Page Builder Support', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Gutenberg Block, Elementor, Bevear Builder, SiteOrigin, Divi, Visual Composer and Fusion Page Builder Support', 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>
		<tr class="aigpl-pro-feature">
			<th>
				<?php _e('Exclude Album and Exclude Some Categories', 'album-and-image-gallery-plus-lightbox'); ?><span class="aigpl-pro-tag"><?php _e('PRO','album-and-image-gallery-plus-lightbox');?></span>
			</th>
			<td>
				<span class="description"><?php _e('Do not display the album & Do not display the album for particular categories.' , 'album-and-image-gallery-plus-lightbox'); ?></span>
			</td>
		</tr>
	</tbody>
</table><!-- end .aigpl-metabox-table -->

