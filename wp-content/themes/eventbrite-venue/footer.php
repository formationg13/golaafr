<?php
/**
 * Template for global footer
 *
 * @package eventbrite-event
 */
?>
	</div>
</section>
<footer class="site-footer row" role="contentinfo">
	<div class="container">
		<?php wp_nav_menu( array(
				'theme_location'  => 'secondary',
				'container_class' => 'pull-right',
				'fallback_cb'     => '__return_false'
			) ); ?>
		<p>
			Powered by:<a class="wordpress-link" href="https://www.facebook.com/chakibdabbek" rel="generator">Chakib Dabbek</a>
		</p>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
