<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'golaafr');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0v=id=0dbvK%`15<-_e8i1f.7)<fB_l%=>|RK.47S&o}_=YE4kZ-Fb+T72T#>J/3');
define('SECURE_AUTH_KEY',  '}XmC!-y `AO;1=u?OAOm%E+|4(cKR8~+t^pd!},P~~0jV$.BBMb9jFFas$~w3gAH');
define('LOGGED_IN_KEY',    '5:}+%L}N[&l@?1^K8j^|e;Qkxocyi@wS9I&@qK+5FQ61G>?geqdZH%74[<FVPFO}');
define('NONCE_KEY',        '>blYN$!-EsGODeq:ZoX|jvD}mN>W`hXml,Q)Y~`*?l@R=s-f+kU@QX(,i-G?)M@&');
define('AUTH_SALT',        'F]nJ:Y|X.GMfm+k`RO&dYc#OVqjR8?fFR]ayAEA2L=jf{[|9 .]N]cmaf|}594Z(');
define('SECURE_AUTH_SALT', 'I[y$e+2M+9}F(l|8EqeBitoa}(2R12Ql+Bwk4zWOwN2w2oI%/4%=RpU k[i|F_u=');
define('LOGGED_IN_SALT',   'v;xULy!^*#hE8ybOSk>y|W 3){6pQe-oh^|ZD.cb(>yYi.oao+)w;V3(7(YZ>rAA');
define('NONCE_SALT',       '1(|Qh{S_fp6Me9[Y-^tC .Lvjw|iD/n` V-Y:poNS0s~pb]P!H^OyPXjDj-1>q5o');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
?>